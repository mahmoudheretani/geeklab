﻿class StoreForm {

    initializeView() {
        this.mapDiv = $("#map");
        this.latitudeInput = $("#Latitude");
        this.longitudeInput = $("#Longitude");
        console.log(this.latitudeInput);
        console.log(this.longitudeInput);
        this.initializeMapBox();
        return this;
    }

    initializeMapBox() {
        mapboxgl.accessToken = 'pk.eyJ1IjoicHJvamVjdHNzeTMiLCJhIjoiY2swZ3N5cWI2MGIycTNobjVmZHkwa3d0ayJ9.Qcg7xiI3XfYi1As82aDpxQ';
        this.map = new mapboxgl.Map({
            //center: [longitude, landitude],
            //zoom: 15,
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v11'
        });
    }

    bindEvents() {
        this.map.on("click", (event) => {
            if (this.marker) {
                this.marker.remove();
            }
            this.marker = new mapboxgl.Marker()
                .setLngLat(event.lngLat)
                .addTo(this.map);
            console.log(event.lngLat);
            //update hidden inputs
            this.latitudeInput.val(event.lngLat.lat);
            this.longitudeInput.val(event.lngLat.lng);
        });
        return this;
    }

}

$(document).ready(() => {
    new StoreForm()
        .initializeView()
        .bindEvents();
});