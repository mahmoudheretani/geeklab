﻿using GeekLab.Models.Base;
using GeekLab.SharedKernal.Data;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace GeekLab.Models.Commerce
{
    [Table("Stores", Schema = Schema.Commerce)]
    public class StoreSet : BaseEntity
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public int CategoryId { get; set; }
        public CategorySet Category { get; set; }
        public ICollection<ProductStoreSet> ProductStores { get; set; }
    }
}
