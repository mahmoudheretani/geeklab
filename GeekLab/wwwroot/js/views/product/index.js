﻿class Index {

    initializeView() {
        this.resultsContainer = $("#resultsContainer");
        return this;
    }
    bindEvents() {
        $(document).on("click", "a.page-link", (event) => {
            event.preventDefault();
            const clickedLink = $(event.target);
            this.loadNextPage(clickedLink.attr("href"));
            return false;
        });
        return this;
    }

    loadNextPage(url) {
        $.ajax({
            url: url,
            type: "GET",
            dataType: "json"
        }).done((json) => {
            this.resultsContainer.html(json);
        }).fail((xhr, status, errorThrown) => {
        }).always((xhr, status) => {
        });
    }
}

$(document).ready(() => {
    new Index()
        .initializeView()
        .bindEvents();
});

