﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeekLab.Utils
{
    public class DefaultUrls
    {
        public const string DefaultProductImageUrl = "/data/productImages/default.png";
    }
}
