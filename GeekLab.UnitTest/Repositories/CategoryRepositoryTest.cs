﻿using GeekLab.Commerce.Data.Repositories;
using GeekLab.Commerce.Dto.Category;
using GeekLab.Models.Commerce;
using GeekLab.SqlServer.Database;
using Microsoft.EntityFrameworkCore;
using System;
using Xunit;

namespace GeekLab.UnitTest.Repositories
{
    public class CategoryRepositoryTest
    {

        public int FirstCategoryId => 100;
        public string FirstCategoryName => "Cat 1";
        public GeekLabDbContext GetDbContext()
        {
            var options = new DbContextOptionsBuilder<GeekLabDbContext>()
               .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
               .Options;
            return new GeekLabDbContext(options);
        }

        public void SeedCategories(GeekLabDbContext context)
        {
            context.Categories.Add(new CategorySet
            {
                Id = FirstCategoryId,
                Name = FirstCategoryName
            });
            context.SaveChanges();
        }


        [Theory(DisplayName = "Get all categories")]
        [InlineData(null)]
        [InlineData("1")]
        public void GetAllCategories(string query)
        {
            var context = GetDbContext();
            SeedCategories(context);
            var categoryRepo = new CategoryRepository(context);
            Assert.Single(categoryRepo.GetCategories(query));
        }

        [Fact(DisplayName = "Get category")]
        public void GetCategoryById()
        {
            var context = GetDbContext();
            SeedCategories(context);
            var categoryRepo = new CategoryRepository(context);
            var category = categoryRepo.GetCategory(FirstCategoryId);
            Assert.Equal(FirstCategoryId, category.Id);
            Assert.Equal(FirstCategoryName, category.Name);
        }

        [Fact(DisplayName = "Add new category")]
        public void AddIngredient()
        {
            var categoryRepo = new CategoryRepository(GetDbContext());
            var result = categoryRepo.SetCategory(new CategoryDto()
            {
                Name = "new category"
            });
            Assert.True(result.IsSuccess);
        }

        [Fact(DisplayName = "Edit category")]
        public void EditCategory()
        {
            var categoryREpo = new CategoryRepository(GetDbContext());
            var editName = "cat 2";
            var result = categoryREpo.SetCategory(new CategoryDto()
            {
                Id = FirstCategoryId,
                Name = editName,
            });
            Assert.True(result.IsSuccess);
        }

        [Fact(DisplayName = "Remove category")]
        public void RemoveIngredient()
        {
            var context = GetDbContext();
            SeedCategories(context);
            var categoryRepo = new CategoryRepository(context);
            categoryRepo.RemoveCategory(FirstCategoryId);
            Assert.Empty(categoryRepo.GetCategories(null));
        }

    }
}
