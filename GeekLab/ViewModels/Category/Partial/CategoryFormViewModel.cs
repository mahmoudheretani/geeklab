﻿using GeekLab.ViewModels.Base;

namespace GeekLab.ViewModels.Category.Partial
{
    public class CategoryFormViewModel : GeekLabViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
