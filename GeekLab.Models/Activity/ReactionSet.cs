﻿using GeekLab.Models.Base;
using GeekLab.Models.Security;
using GeekLab.SharedKernal.Data;
using GeekLab.SharedKernal.Data.Enums;
using System.ComponentModel.DataAnnotations.Schema;

namespace GeekLab.Models.Activity
{
    [Table("Reactions", Schema = Schema.Activity)]
    public class ReactionSet : BaseEntity
    {
        public string Comment { get; set; }
        public string Text { get; set; }
        public ReactionType ReactionType { get; set; }
        public int UserId { get; set; }
        public GLUser User { get; set; }
    }
}
