﻿using GeekLab.Commerce.Dto.Category;
using GeekLab.ViewModels.Base;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeekLab.ViewModels.Store
{
    public class StoreFormViewModel:GeekLabViewModel
    {
        public StoreFormViewModel()
        {
            PageTitle = "Store form";
        }
        [HiddenInput]
        public int Id { get; set; }
        public string Name { get; set; }
        public int CategoryId { get; set; }
        public string Address { get; set; }
        [HiddenInput]
        public double Longitude { get; set; }
        [HiddenInput]
        public double Latitude { get; set; }
        public IEnumerable<CategoryDto> Categories { get; set; }
    }
}
