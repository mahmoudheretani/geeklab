﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeekLab.ViewModels.Base
{
    public abstract class GeekLabViewModel
    {
        public string PageTitle { get; set; }
    }
}
