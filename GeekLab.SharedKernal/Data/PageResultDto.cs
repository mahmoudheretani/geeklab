﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GeekLab.SharedKernal.Data
{
    public  class PageResultDto<TResult>
    {
        public IEnumerable<TResult> Results { get; set; }
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        public int TotalResultsCount { get; set; }
    }
}
