﻿using GeekLab.BoundedContext.Shared.Repositories;
using GeekLab.Commerce.Dto.Store;
using GeekLab.Commerce.IData.Interfaces;
using GeekLab.SqlServer.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GeekLab.Commerce.Data.Repositories
{
    public class StoreRepository : GeekLabRepository,IStoreRepository
    {
        public StoreRepository(GeekLabDbContext context) : base(context)
        {
        }

        public bool SetStore(StoreFormDto storeFormDto)
        {
            try
            {
                var storeEntity = Context.Stores
                    .SingleOrDefault(store => store.Id == storeFormDto.Id);
                if (storeEntity == null)
                {
                    //add case
                    storeEntity = new Models.Commerce.StoreSet();
                    Context.Attach(storeEntity);
                }
                storeEntity.Name = storeFormDto.Name;
                storeEntity.CategoryId = storeFormDto.CategoryId;
                storeEntity.Address = storeFormDto.Address;
                storeEntity.Latitude = storeFormDto.Latitude;
                storeEntity.Longitude = storeFormDto.Longitude;
                Context.SaveChanges();
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }
    }
}
