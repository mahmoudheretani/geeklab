﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeekLab.Utils
{
    public static  class Sections
    {
        public const string Scripts = nameof(Scripts);
        public const string Styles = nameof(Styles);
        public const string Modals = nameof(Modals);
    }
}
