﻿namespace GeekLab.SharedKernal.Data.Enums
{
    public enum ReactionType
    {
        Like,
        Love,
        Hate
    }
}
