﻿using GeekLab.Security.Dto.User;
using System;
using System.Threading.Tasks;

namespace GeekLab.Security.IData
{
    public interface IAccountRepository
    {
        Guid GetUserId();
        Task<UserInfoDto> Login(LoginDto loginDto);
        Task<UserInfoDto> CreateAccount(UserInfoDto userInfoDto);
        Task Logout();
    }
}
