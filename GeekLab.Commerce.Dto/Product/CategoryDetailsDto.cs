﻿using System.Collections.Generic;

namespace GeekLab.Commerce.Dto.Product
{
    public class CategoryDetailsDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<ProductDto> Products { get; set; }
    }
}
