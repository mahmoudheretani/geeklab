﻿export class AjaxUtil {


    static get LoadingIconClass() {
        return ".btn-loading-icon";
    }

    static get BtnIconClass() {
        return ".btn-icon";
    }

    static get BtnTextClass() {
        return ".btn-text";
    }

    static displayLoading(btn) {
        btn.find(AjaxUtil.BtnTextClass).hide();
        btn.find(AjaxUtil.BtnIconClass).hide();
        btn.find(AjaxUtil.LoadingIconClass).show();
    }

    static hideLoading(btn) {
        btn.find(AjaxUtil.BtnTextClass).show();
        btn.find(AjaxUtil.BtnIconClass).show();
        btn.find(AjaxUtil.LoadingIconClass).hide();
    }


}