﻿using GeekLab.Commerce.Dto.Category;
using GeekLab.SharedKernal.Data;
using System.Collections.Generic;

namespace GeekLab.Commerce.IData.Interfaces
{
    public interface ICategoryRepository
    {
        IEnumerable<CategoryDto> GetCategories(string query = null);
        CategoryDto GetCategory(int id);
        OperationResult<CategoryDto> SetCategory(CategoryDto categoryFormDto);
        OperationResult<int> RemoveCategory(int id);
    }
}
