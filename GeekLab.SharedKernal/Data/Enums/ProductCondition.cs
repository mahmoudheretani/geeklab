﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GeekLab.SharedKernal.Data.Enums
{
    public enum ProductCondition
    {
        New,
        Used,
        Old
    }
}
