﻿using GeekLab.Commerce.Dto.Product;
using GeekLab.SharedKernal.Data;
using GeekLab.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace GeekLab.ViewModels.Product
{
    public class IndexViewModel:GeekLabViewModel
    {
        public IPagedList<ProductDto> ProductPagedList { get; set; }
        public PageResultDto<ProductDto> PageResult { get; set; }
        public IndexViewModel()
        {
            PageTitle = "Products";
        }
    }
}
