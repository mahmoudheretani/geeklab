﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeekLab.Utils
{
    public static class PathsViews
    {
        #region Layouts
        public const string _PageHeader = "~/Views/Shared/Partials/_PageHeader.cshtml";
        public const string _SharedStyles = "~/Views/Shared/Partials/_SharedStyles.cshtml";
        public const string _SharedScripts = "~/Views/Shared/Partials/_SharedScripts.cshtml";
        public const string _Header = "~/Views/Shared/Partials/_Header.cshtml";
        public const string _Footer = "~/Views/Shared/Partials/_Footer.cshtml";
        #endregion

        #region Categories

        public const string _CategoryRow = "~/Views/Category/Partials/_CategoryRow.cshtml";
        public const string _CategoryFormModel = "~/Views/Category/Partials/_CategoryFormModel.cshtml";

        #endregion

        #region Products

        public const string _ProductCard = "~/Views/Product/Partials/_ProductCard.cshtml";
        public const string _ProductsResults = "~/Views/Product/Partials/_ProductsResults.cshtml";

        #endregion

    }
}
