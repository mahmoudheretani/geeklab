﻿using GeekLab.Models.Commerce;
using GeekLab.Models.Security;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace GeekLab.SqlServer.Database
{
    public class GeekLabDbContext : IdentityDbContext<GLUser, GLRole, int, GLUserClaim, GLUserRole
                                                     , GLUserLogin, GLRoleClaim, GLUserToken>
    {

        #region Properties

        public DbSet<ProductSet> Products { get; set; }
        public DbSet<CategorySet> Categories { get; set; }
        public DbSet<StoreSet> Stores { get; set; }

        #endregion

        #region Constructors
        public GeekLabDbContext(DbContextOptions<GeekLabDbContext> options)
            : base(options)
        {

        }
        #endregion

        #region Methods

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            DisableCascade(modelBuilder);
            SeedData(modelBuilder);
        }

        private void DisableCascade(ModelBuilder modelBuilder)
        {
            var cascadeFKs = modelBuilder.Model.GetEntityTypes()
       .SelectMany(t => t.GetForeignKeys())
       .Where(fk => !fk.IsOwnership && fk.DeleteBehavior == DeleteBehavior.Cascade);

            foreach (var fk in cascadeFKs)
                fk.DeleteBehavior = DeleteBehavior.Restrict;

        }

        private void SeedData(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CategorySet>().HasData(new CategorySet
            {
                Id = 1,
                Name = "Electronics",
                IsValid = true
            });
            modelBuilder.Entity<CategorySet>().HasData(new CategorySet
            {
                Id = 2,
                Name = "Laptops",
                IsValid = true
            });
            modelBuilder.Entity<CategorySet>().HasData(new CategorySet
            {
                Id = 3,
                Name = "Mobiles",
                IsValid = true
            });

            modelBuilder.Entity<ProductSet>().HasData(new ProductSet
            {
                Id = 1,
                CategoryId = 1,
                Name = "Blender",
                AddDate = new System.DateTimeOffset(new System.DateTime(2018, 5, 1)),
                Description = "Great blender for any type of foods",
                ImageUrl = "/data/productImages/1.jpg",
                ShowProduct = true,
                IsValid = true
            });
            modelBuilder.Entity<ProductSet>().HasData(new ProductSet
            {
                Id = 2,
                CategoryId = 2,
                Name = "ASUS Zenbook UX430UA",
                AddDate = new System.DateTimeOffset(new System.DateTime(2017, 5, 1)),
                Description = "Wonderful laptop",
                ImageUrl = "/data/productImages/2.jpg",
                ShowProduct = true,
                IsValid = true
            });
        }

        #endregion

    }
}
