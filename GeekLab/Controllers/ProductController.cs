﻿using GeekLab.Commerce.Dto.Product;
using GeekLab.Commerce.IData.Interfaces;
using GeekLab.Utils;
using GeekLab.ViewModels.Product;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace GeekLab.Controllers
{
    public class ProductController : Controller
    {


        private const int DefaultPageNumber = 1;
        private const int DefaultPageSize = 1;
        private IProductRepository ProductRepository { get; }
        private ICategoryRepository CategoryRepository { get; }
        public ProductController(IProductRepository productRepository
            , ICategoryRepository categoryRepository)
        {
            ProductRepository = productRepository;
            CategoryRepository = categoryRepository;
        }
        public async Task<IActionResult> Index(int? pageNumber, int? pageSize, string query)
        {
            //if (pageNumber == null)
            //{
            //    pageNumber = 1;
            //}
            pageNumber = pageNumber ?? DefaultPageNumber;
            pageSize = pageSize ?? DefaultPageSize;

            var pageResult = ProductRepository.GetProductsPage
                (pageNumber.Value - 1, pageSize.Value, query);


            var productPagedList = new StaticPagedList<ProductDto>
                (
                    pageResult.Results,
                    pageNumber.Value,
                    pageSize.Value,
                    pageResult.TotalResultsCount
                );
            var vm = new IndexViewModel()
            {
                PageResult = pageResult,
                ProductPagedList = productPagedList
            };
            if (Request.IsAjaxRequest())
            {
                var partialAsString = await this.RenderViewAsync(PathsViews._ProductsResults, vm, true);
                //ajax return
                return Json(partialAsString);
            }
            return View(vm);
        }

        public IActionResult ProductForm()
        {
            var vm = new ProductFormViewModel()
            {
                Categories = CategoryRepository.GetCategories()
                .Select(catDto => new SelectListItem()
                {
                    Value = catDto.Id.ToString(),
                    Text = catDto.Name
                }).ToList()
            };
            return View(vm);
        }

        public IActionResult ViewProduct(int id)
        {
            return null;
        }

        public IActionResult ViewProductPosts(int id)
        {
            return null;
        }

        [HttpPost]
        public IActionResult ProductForm(ProductFormViewModel productFormViewModel)
        {
            //var productName = HttpContext.Request.Form["productName"];
            //var description = HttpContext.Request.Form["description"];
            //   return Content($"Product saved successfully {productFormViewModel.Name} {productFormViewModel.Description}");
            //return View("~/Product/ProductForm", productFormViewModel);

            var setResult = ProductRepository.SetProduct(new ProductFormDto()
            {
                Id = productFormViewModel.Id,
                Name = productFormViewModel.Name,
                AddDate = productFormViewModel.AddDate,
                Description = productFormViewModel.Description,
                ShowProduct = productFormViewModel.ShowProduct,
                SelectedCategory = productFormViewModel.SelectedCategory,
                ProductCondition = productFormViewModel.SelectedProductCondition
            });
            if (setResult)
            {
                return RedirectToAction(nameof(Index));
            }
            return View(productFormViewModel);
        }
    }
}
