﻿using GeekLab.ViewModels.Home;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace GeekLab.Controllers
{
    public class HomeController : Controller
    {
        #region Properties

        private ILogger<HomeController> Logger { get; }

        #endregion

        #region Constructors

        public HomeController(ILogger<HomeController> logger)
        {
            Logger = logger;
            logger.LogInformation("Hello world from logs");
        }

        #endregion

        #region Methods

        public IActionResult Index()
        {
            var vm = new IndexViewModel();
            return View(vm);
        }

        #endregion

    }
}
