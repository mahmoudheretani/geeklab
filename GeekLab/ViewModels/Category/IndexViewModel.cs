﻿using GeekLab.Commerce.Dto.Category;
using GeekLab.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeekLab.ViewModels.Category
{
    public class IndexViewModel:GeekLabViewModel
    {
        public IndexViewModel()
        {
            PageTitle = "Categories";
        }
        public IEnumerable<CategoryDto> Categories { get; set; }

    }
}
