﻿using GeekLab.BoundedContext.Shared.Repositories;
using GeekLab.Commerce.Dto.Category;
using GeekLab.Commerce.IData.Interfaces;
using GeekLab.SharedKernal.Data;
using GeekLab.SqlServer.Database;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GeekLab.Commerce.Data.Repositories
{
    public class CategoryRepository : GeekLabRepository, ICategoryRepository
    {
        public CategoryRepository(GeekLabDbContext context)
            : base(context)
        {
        }

        public IEnumerable<CategoryDto> GetCategories(string query = null)
        {
            return Context.Categories
                     .Where(cat => cat.IsValid
                        &&
                        (
                            string.IsNullOrEmpty(query)
                            ||
                            query.ToUpper().Contains(cat.Name.ToUpper())
                        )
                     )
                     .OrderBy(cat => cat.Name)
                     .Select(cat => new CategoryDto()
                     {
                         Id = cat.Id,
                         Name = cat.Name,
                         ProductsCount = cat.Products.Count(prod => prod.IsValid),
                         StoresCount = cat.Stores.Count(store => store.IsValid)
                     }).ToList();
        }

        public CategoryDto GetCategory(int id)
        {
            return Context.Categories
                    .Where(cat => cat.IsValid && cat.Id == id)
                    .Select(cat => new CategoryDto()
                    {
                        Id = cat.Id,
                        Name = cat.Name,
                        ProductsCount = cat.Products.Count(prod => prod.IsValid),
                        StoresCount = cat.Stores.Count(store => store.IsValid)
                    }).SingleOrDefault();
        }

        public OperationResult<CategoryDto> SetCategory(CategoryDto categoryFormDto)
        {
            var operationResult = new OperationResult<CategoryDto>()
            {
                ResultData = categoryFormDto
            };
            try
            {
                var isCategoryExists = Context.Categories
                .Any(cat => cat.Name.ToUpper() == categoryFormDto.Name.ToUpper()
                                    && categoryFormDto.Id == 0
                    );
                if (isCategoryExists)
                {
                    return operationResult;
                }
                var categoryEntity = Context.Categories
                    .SingleOrDefault(cat => cat.Id == categoryFormDto.Id);
                if (categoryEntity == null)
                {
                    categoryEntity = new Models.Commerce.CategorySet();
                    Context.Attach(categoryEntity);
                }
                categoryEntity.Name = categoryFormDto.Name;
                Context.SaveChanges();
                operationResult.IsSuccess = true;
                operationResult.ResultData = GetCategory(categoryEntity.Id);
                return operationResult;
            }
            catch (Exception ex)
            {
                operationResult.Exception = ex;
            }
            return operationResult;

        }

        public OperationResult<int> RemoveCategory(int id)
        {
            var operationResult = new OperationResult<int>()
            {
                ResultData = id
            };
            try
            {
                var categoryEntity = Context.Categories
                    .SingleOrDefault(cat => cat.Id == id && cat.IsValid);
                if (categoryEntity == null)
                {
                    operationResult.IsSuccess = true;
                    return operationResult;
                }
                categoryEntity.IsValid = false;
                Context.SaveChanges();
                operationResult.IsSuccess = true;
                return operationResult;
            }
            catch (Exception ex)
            {
                operationResult.Exception = ex;
            }
            return operationResult;
        }
    }
}
