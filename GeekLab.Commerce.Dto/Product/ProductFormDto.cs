﻿using GeekLab.SharedKernal.Data.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace GeekLab.Commerce.Dto.Product
{
    public class ProductFormDto: ProductDto
    {
        public string Description { get; set; }
        public DateTime AddDate { get; set; }
        public bool ShowProduct { get; set; }
        public int SelectedCategory { get; set; }
        public ProductCondition ProductCondition { get; set; }
    }
}
