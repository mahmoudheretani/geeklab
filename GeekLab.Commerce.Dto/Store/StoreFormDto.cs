﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GeekLab.Commerce.Dto.Store
{
    public  class StoreFormDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CategoryId { get; set; }
        public string Address { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
    }
}
