﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GeekLab.Commerce.Dto.Store;
using GeekLab.Commerce.IData.Interfaces;
using GeekLab.Utils;
using GeekLab.ViewModels.Store;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GeekLab.Controllers
{
    public class StoreController : Controller
    {
        private ICategoryRepository CategoryRepository { get; }
        private IStoreRepository StoreRepository { get; }

        public StoreController(ICategoryRepository categoryRepository
            , IStoreRepository storeRepository)
        {
            CategoryRepository = categoryRepository;
            StoreRepository = storeRepository;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult StoreForm(int?id)
        {
            var vm = new StoreFormViewModel()
            {
                Categories = CategoryRepository.GetCategories()
            };
            return View(vm);
        }

        [HttpPost]
        public IActionResult StoreForm(StoreFormViewModel storeFormViewModel)
        {
            var setResult = StoreRepository.SetStore(new StoreFormDto()
            {
                Id=storeFormViewModel.Id,
                CategoryId=storeFormViewModel.CategoryId,
                Address=storeFormViewModel.Address,
                Latitude= storeFormViewModel.Latitude,
                Longitude=storeFormViewModel.Longitude,
                Name=storeFormViewModel.Name
            });
            if (setResult)
            {
                return RedirectToAction
                    (nameof(HomeController.Index), nameof(HomeController).ToActionName());
            }
            return View(storeFormViewModel);
        }
    }
}
