﻿using GeekLab.SharedKernal.Data.Enums;
using System;

namespace GeekLab.Commerce.Dto.Product
{
    public class ProductDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public string CategoryName { get; set; }
        public DateTimeOffset AddDate { get; set; }
        public ProductCondition ProductCondition { get; set; }
        public int PostsCount { get; set; }
    }
}
