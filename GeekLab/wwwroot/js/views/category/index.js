﻿import { AjaxUtil } from "/js/views/util/ajaxUtil.js";
class Index {

    initializeView() {
        this.addCategory = $("#addCategory");
        this.categoryTable = $("#categoryTable");
       this.categoryTable.DataTable();
        this.getCategoryUrl = this.categoryTable.data("get-url");
        this.removeCategoryUrl = this.categoryTable.data("remove-url");
        this.initializeCategoryModal();

        this.editRow = null;

        return this;
    }

    initializeCategoryModal() {
        this.categoryFormModal = $("#categoryFormModal");
        this.categoryForm = this.categoryFormModal.find("#categoryForm");

        this.categoryFormModalName = this.categoryFormModal.find("#categoryFormModalName");
        this.categoryFormModalId = this.categoryFormModal.find("#categoryFormModalId");

        this.categoryFormUrl = this.categoryForm.attr("action");
        this.categoryFormModalSaveBtn = this.categoryForm.find("#categoryFormModalSaveBtn");
    }

    bindEvents() {
        this.categoryForm.submit((event) => {
            event.preventDefault();
            this.submitCategoryForm();
            return false;
        });
        this.categoryFormModalSaveBtn.click((event) => {
            event.preventDefault();
            this.submitCategoryForm();
            return false;
        });
        this.addCategory.click((event) => {
            event.preventDefault();
            this.editRow = null;
            this.categoryFormModal.modal("show");
            return false;
        });

        $(document).on('click', '.edit-category', (e) => {
            this.editRow = $(e.target).closest("tr");
            this.fillEditCategoryModal();
            return false;
        });

        $(document).on('click', '.remove-category', (e) => {
            this.removedRow = $(e.target).closest("tr");
            this.removeCategory();
            return false;
        });

        /*
            Remove category
         */

        return this;
    }

    submitCategoryForm() {

        const categoryFormData = new FormData(this.categoryForm[0]);
        AjaxUtil.displayLoading(this.categoryFormModalSaveBtn);
        $.ajax({
            url: this.categoryFormUrl,
            data: categoryFormData,
            type: "POST",
            dataType: "json",
            processData: false,
            contentType: false
        }).done((json) => {
            this.actionCategoryRow(json);
        }).fail((xhr, status, errorThrown) => {
        }).always((xhr, status) => {
            AjaxUtil.hideLoading(this.categoryFormModalSaveBtn);
        });
    }

    actionCategoryRow(json) {
        console.log(json);
        if (!this.editRow) {
            this.categoryTable.find("tbody").append(this.getCategoryRow(json.resultData));
            iziToast.success({
                title: 'Add category',
                message: `${json.resultData.name} category added successfully`
            });
        } else {
            this.editRow.find(".categoryName").html(json.resultData.name);
            iziToast.success({
                title: 'Edit category',
                message: `${json.resultData.name} category updated successfully`
            });
        }
        this.categoryFormModal.modal("hide");
    }

    fillEditCategoryModal() {
        const categoryId = this.editRow.data("id");
        $.ajax({
            url: this.getCategoryUrl,
            data: {
                id: categoryId
            },
            type: "GET",
            dataType: "json"
        }).done((json) => {
            this.categoryFormModalName.val(json.name);
            this.categoryFormModalId.val(categoryId);
            this.categoryFormModal.modal("show");
        }).fail((xhr, status, errorThrown) => {
            iziToast.error({
                title: 'Error',
                message: 'Failed to get category data, please try again later'
            });
        }).always((xhr, status) => {
        });

    }

    getCategoryRow(json) {
        return ` <tr data-id="${json.id}">
                    <td class="categoryName">${json.name}</td>
                    <td>${json.productsCount}</td>
                    <td>${json.storesCount}</td>
                    <td>
                        <a class="btn btn-info btn-sm edit-category"
                           data-toggle="tooltip"
                           href="#"
                           data-placement="top"
                           title="Edit category">
                            <i class="fas fa-edit"></i>
                        </a>
                        <a class="btn btn-danger btn-sm remove-category"
                           data-toggle="tooltip"
                           data-placement="top"
                           href="#"
                           title="Remove category">
                            <i class="fas fa-trash"></i>
                        </a>
                    </td>
                </tr>`;
    }

    removeCategory() {
        const categoryId = this.removedRow.data("id");
        $.ajax({
            url: this.removeCategoryUrl,
            data: {
                id: categoryId
            },
            type: "DELETE",
            dataType: "json"
        }).done((json) => {
            this.removedRow.remove();
            iziToast.success({
                title: 'Remove category',
                message: 'Category removed successfully'
            });
        }).fail((xhr, status, errorThrown) => {
            iziToast.error({
                title: 'Error',
                message: 'Failed to get category data, please try again later'
            });
        }).always((xhr, status) => {
        });
    }
}

$(document).ready(() => {
    new Index()
        .initializeView()
        .bindEvents();
})