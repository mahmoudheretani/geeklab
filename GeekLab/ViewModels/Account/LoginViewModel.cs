﻿using GeekLab.ViewModels.Base;
using System.ComponentModel.DataAnnotations;

namespace GeekLab.ViewModels.Account
{
    public class LoginViewModel : GeekLabViewModel
    {
        #region Properties

        [Required(ErrorMessage = "Please enter the username/email")]
        public string UserName { get; set; }
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Please enter the password")]
        public string Password { get; set; }
        public bool RememberMe { get; set; }

        #endregion

        #region Constructors

        #endregion

        #region Methods

        #endregion
    }
}