﻿using GeekLab.BoundedContext.Shared.Repositories;
using GeekLab.Models.Security;
using GeekLab.Security.Dto.User;
using GeekLab.Security.IData;
using GeekLab.SqlServer.Database;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PGeekLab.Security.Data.Repositories
{
    public class AccountRepository : GeekLabRepository, IAccountRepository
    {
        public UserManager<GLUser> UserManager { get; }
        public SignInManager<GLUser> SignInManager { get; }
        public HttpContext HttpContext { get; }

        public AccountRepository(GeekLabDbContext context
            , IHttpContextAccessor httpContextAccessor
            , UserManager<GLUser> userManager
            , SignInManager<GLUser> signInManager)
            : base(context)
        {
            UserManager = userManager;
            SignInManager = signInManager;
            HttpContext = httpContextAccessor.HttpContext;
        }


        public Guid GetUserId()
        {
            try
            {
                if (HttpContext != null
                    && HttpContext.User != null
                    && HttpContext.User.Identity != null
                    && HttpContext.User.Identity.IsAuthenticated)
                {
                    return Guid.Parse(HttpContext.User
                        .FindFirstValue(ClaimTypes.NameIdentifier));
                }
            }
            catch (Exception ex)
            {

            }
            return Guid.Empty;
        }

        public async Task<UserInfoDto> Login(LoginDto loginDto)
        {
            if (!Context.Users.Any())
            {
                await CreateAccount(new UserInfoDto()
                {
                    Email = "admin@admin.com",
                    Password = "Aaa@123",
                    Username = "admin"
                });
            }
            var userEntity = Context.Users
                .SingleOrDefault(user => loginDto.UserName == user.UserName
                                ||
                                user.Email.ToUpper() == loginDto.UserName.ToUpper());
            if (userEntity == null)
            {
                return null;
            }
            var loginResult = await SignInManager.PasswordSignInAsync(userEntity, loginDto.Password, loginDto.RememberMe, false);
            if (loginResult == SignInResult.Success)
            {
                return new UserInfoDto()
                {
                    Id = userEntity.Id,
                    Username = userEntity.UserName,
                    Email = userEntity.Email
                };
            }
            return null;
        }

        public async Task<UserInfoDto> CreateAccount(UserInfoDto userInfoDto)
        {
            try
            {
                var result = await UserManager.CreateAsync(new GLUser()
                {
                    UserName = userInfoDto.Username,
                    Email = userInfoDto.Email,
                    EmailConfirmed = true
                }, userInfoDto.Password);
                if (result == IdentityResult.Success)
                {
                    return userInfoDto;
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        public async Task Logout()
        {
            await SignInManager.SignOutAsync();
        }
    }
}
