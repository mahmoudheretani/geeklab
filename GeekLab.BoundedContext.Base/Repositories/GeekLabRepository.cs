﻿using GeekLab.SqlServer.Database;

namespace GeekLab.BoundedContext.Shared.Repositories
{
    public abstract class GeekLabRepository
    {
        protected GeekLabDbContext Context { get; }
        public GeekLabRepository(GeekLabDbContext context)
        {
            Context = context;
        }
    }
}
