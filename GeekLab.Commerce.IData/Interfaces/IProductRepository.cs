﻿using GeekLab.Commerce.Dto.Product;
using GeekLab.SharedKernal.Data;
using System.Collections.Generic;

namespace GeekLab.Commerce.IData.Interfaces
{
    public interface IProductRepository
    {
        IEnumerable<ProductDto> GetProducts(string query = null);
        IEnumerable<CategoryDetailsDto> GetCategorizedProduct();
        bool SetProduct(ProductFormDto productFormDto);
        PageResultDto<ProductDto> GetProductsPage(int pageNumber = 0, int pageSize = 10, string query = null);
    }
}
