﻿using GeekLab.Models.Base;
using GeekLab.SharedKernal.Data;
using GeekLab.SharedKernal.Data.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace GeekLab.Models.Commerce
{
    [Table("Products", Schema = Schema.Commerce)]
    public class ProductSet : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTimeOffset AddDate { get; set; }
        public bool ShowProduct { get; set; }
        public string ImageUrl { get; set; }
        public ProductCondition ProductCondition { get; set; }
        public int CategoryId { get; set; }
        public CategorySet Category { get; set; }
        public ICollection<ProductStoreSet> ProductStores { get; set; }
    }
}
