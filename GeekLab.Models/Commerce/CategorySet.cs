﻿using GeekLab.Models.Base;
using GeekLab.SharedKernal.Data;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace GeekLab.Models.Commerce
{
    [Table("Categories", Schema = Schema.Commerce)]
    public class CategorySet : BaseEntity
    {
        public string Name { get; set; }
        public ICollection<StoreSet> Stores { get; set; }
        public ICollection<ProductSet> Products { get; set; }
    }
}
