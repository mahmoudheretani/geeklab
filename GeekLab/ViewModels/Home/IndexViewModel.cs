﻿using GeekLab.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeekLab.ViewModels.Home
{
    public class IndexViewModel:GeekLabViewModel
    {
        public IndexViewModel()
        {
            PageTitle = "Home";
        }
    }
}
