﻿using GeekLab.Models.Activity;
using GeekLab.Models.Base;
using GeekLab.SharedKernal.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace GeekLab.Models.Commerce
{
    [Table("ProductStores", Schema = Schema.Commerce)]
    public class ProductStoreSet : BaseEntity
    {
        public double Price { get; set; }
        public DateTimeOffset Date { get; set; }
        public int StoreId { get; set; }
        public StoreSet Store { get; set; }
        public int ProductId { get; set; }
        public ProductSet Product { get; set; }
        public ICollection<PostSet> Posts { get; set; }
    }
}
