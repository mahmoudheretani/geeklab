﻿using GeekLab.Commerce.Data.Repositories;
using GeekLab.Commerce.IData.Interfaces;
using GeekLab.Models.Security;
using GeekLab.Security.IData;
using GeekLab.SqlServer.Database;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using PGeekLab.Security.Data.Repositories;
using Serilog;

namespace GeekLab
{
    public class Startup
    {

        #region Properties
        private IConfiguration Configuration { get; }
        private const string DefaultConnection = nameof(DefaultConnection);
        private const string ServerConnection = nameof(ServerConnection);

        #endregion

        #region Constructors

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        #endregion

        #region Methods

        public void ConfigureServices(IServiceCollection services)
        {

            services.AddDbContext<GeekLabDbContext>
                (options =>
                {
                    options.UseSqlServer
                            (Configuration.GetConnectionString(ServerConnection));
                });
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            ConfigureGeekLabRepositories(services);
            services.AddIdentity<GLUser, GLRole>()
                .AddEntityFrameworkStores<GeekLabDbContext>();
            services.AddHttpContextAccessor();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        private void ConfigureGeekLabRepositories(IServiceCollection services)
        {
            services.AddScoped<IProductRepository, ProductRepository>();
            services.AddScoped<ICategoryRepository, CategoryRepository>();
            services.AddScoped<IStoreRepository, StoreRepository>();
            services.AddScoped<IAccountRepository, AccountRepository>();
        }
        public void Configure(IApplicationBuilder app
            , IHostingEnvironment env
            , ILoggerFactory loggerfactory
            , IApplicationLifetime appLifetime)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            loggerfactory.AddSerilog();
            appLifetime.ApplicationStopped.Register(Log.CloseAndFlush);


            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseAuthentication();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }

        #endregion

    }
}
