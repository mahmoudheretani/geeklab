﻿using GeekLab.Commerce.Dto.Category;
using GeekLab.Commerce.IData.Interfaces;
using GeekLab.ViewModels.Category;
using GeekLab.ViewModels.Category.Partial;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net;


namespace GeekLab.Controllers
{
    [Authorize]
    public class CategoryController : Controller
    {
        private ICategoryRepository CategoryRepository { get; }

        public CategoryController(ICategoryRepository categoryRepository)
        {

            CategoryRepository = categoryRepository;
        }
        public IActionResult Index()
        {
            var vm = new IndexViewModel()
            {
                Categories = CategoryRepository.GetCategories()
            };
            return View(vm);
        }

        public IActionResult GetCategory(int id)
        {
            return Json(CategoryRepository.GetCategory(id));
        }

        [HttpPost]
        public IActionResult CategoryForm(CategoryFormViewModel categoryFormViewModel)
        {
            var setResult = CategoryRepository.SetCategory(new CategoryDto()
            {
                Id = categoryFormViewModel.Id,
                Name = categoryFormViewModel.Name
            });
            if (!setResult.IsSuccess)
            {
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return Json(setResult);
        }

        [HttpDelete]
        public IActionResult RemoveCategory(int id)
        {
            var removeResult = CategoryRepository.RemoveCategory(id);
            if (!removeResult.IsSuccess)
            {
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return Json(removeResult);
        }
    }
}
