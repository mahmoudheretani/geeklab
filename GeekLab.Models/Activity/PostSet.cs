﻿using GeekLab.Models.Base;
using GeekLab.Models.Commerce;
using GeekLab.Models.Security;
using GeekLab.SharedKernal.Data;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace GeekLab.Models.Activity
{
    [Table("Posts", Schema = Schema.Activity)]
    public class PostSet : BaseEntity
    {
        public string Title { get; set; }
        public string Text { get; set; }
        public DateTimeOffset DateTime { get; set; }

        public int UserId { get; set; }
        public GLUser User { get; set; }
        public int ProductStoreId { get; set; }
        public ProductStoreSet ProductStore { get; set; }
    }
}
