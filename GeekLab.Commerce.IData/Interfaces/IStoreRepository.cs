﻿using GeekLab.Commerce.Dto.Store;
using System;
using System.Collections.Generic;
using System.Text;

namespace GeekLab.Commerce.IData.Interfaces
{
    public interface IStoreRepository
    {
        bool SetStore(StoreFormDto storeFormDto);
    }
}
