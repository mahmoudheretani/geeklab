﻿namespace GeekLab.Commerce.Dto.Category
{
    public class CategoryDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ProductsCount { get; set; }
        public int StoresCount { get; set; }
    }
}
