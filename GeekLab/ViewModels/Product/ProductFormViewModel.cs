﻿using GeekLab.SharedKernal.Data.Enums;
using GeekLab.ViewModels.Base;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GeekLab.ViewModels.Product
{
    public class ProductFormViewModel: GeekLabViewModel
    {
        public ProductFormViewModel()
        {
            PageTitle = "Product form";
        }
        [HiddenInput]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool ShowProduct { get; set; }
        [DataType(DataType.Date)]
        [Display(Name ="Add date")]
        public DateTime AddDate { get; set; } = DateTime.Today;
        public IEnumerable<SelectListItem> Categories { get; set; }
        public int SelectedCategory { get; set; }
        public ProductCondition SelectedProductCondition { get; set; }

    }
}
