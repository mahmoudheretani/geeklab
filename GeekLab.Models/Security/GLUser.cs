﻿using GeekLab.Models.Activity;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace GeekLab.Models.Security
{
    public class GLUser : IdentityUser<int>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public ICollection<ReactionSet> Reactions { get; set; }
        public ICollection<PostSet> Posts { get; set; }

    }
}
