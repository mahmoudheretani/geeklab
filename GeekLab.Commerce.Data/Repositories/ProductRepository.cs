﻿using GeekLab.Commerce.Dto.Product;
using GeekLab.Commerce.IData.Interfaces;
using GeekLab.SharedKernal.Data;
using GeekLab.SqlServer.Database;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GeekLab.Commerce.Data.Repositories
{
    public class ProductRepository : IProductRepository
    {

        #region Properties

        private GeekLabDbContext Context { get; }

        #endregion

        #region Constructors

        public ProductRepository(GeekLabDbContext geekLabDbContext)
        {
            Context = geekLabDbContext;
        }

        #endregion

        #region Methods

        public IEnumerable<ProductDto> GetProducts(string query = null)
        {
            var results = Context.Products
                .Where(product => string.IsNullOrEmpty(query) || product.Name.Contains(query))
                .Select(product => new ProductDto()
                {
                    Id = product.Id,
                    Name = product.Name,
                    ProductCondition=product.ProductCondition,
                    CategoryName=product.Category.Name,
                    PostsCount = product.ProductStores.Sum(productStore => productStore.Posts.Count)
                }).ToList();
            return results;
        }

        public PageResultDto<ProductDto> GetProductsPage(int pageNumber=0,int pageSize=10,string query = null)
        {
            var productsQuery = Context.Products
                .Where(product => 
                product.IsValid 
                &&
                       ( 
                            string.IsNullOrEmpty(query) 
                            ||
                            product.Name.Contains(query)
                        )
                );
            var pageResult = new PageResultDto<ProductDto>()
            {
                PageNumber=pageNumber,
                PageSize=pageSize,
                TotalResultsCount= productsQuery.Count()
            };
            pageResult.Results= productsQuery.OrderBy(product=>product.Name)
                .Skip(pageNumber*pageSize)
                .Take(pageSize)
                .Select(product => new ProductDto()
                {
                    Id = product.Id,
                    Name = product.Name,
                    AddDate=product.AddDate,
                    ImageUrl=product.ImageUrl,
                    ProductCondition = product.ProductCondition,
                    CategoryName = product.Category.Name,
                    PostsCount = product.ProductStores.Sum(productStore => productStore.Posts.Count)
                }).ToList();
            return pageResult;
        }

        public bool SetProduct(ProductFormDto productFormDto)
        {
            try
            {
                var productEntity = Context.Products
                    .SingleOrDefault(product => product.Id == productFormDto.Id);
                if (productEntity == null)
                {
                    //add
                    productEntity = new Models.Commerce.ProductSet();
                    Context.Attach(productEntity);
                }
                productEntity.Name = productFormDto.Name;
                productEntity.Description = productFormDto.Description;
                productEntity.AddDate = productFormDto.AddDate;
                productEntity.ShowProduct = productFormDto.ShowProduct;
                productEntity.CategoryId = productFormDto.SelectedCategory;
                productEntity.ProductCondition = productFormDto.ProductCondition;
                Context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public IEnumerable<CategoryDetailsDto> GetCategorizedProduct()
        {
            throw new NotImplementedException();
        }

        #endregion

    }
}
